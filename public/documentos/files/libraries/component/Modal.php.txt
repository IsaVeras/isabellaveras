<?php
include_once 'Component.php';

class ModalL extends Component{
    /**
   * Classe cria o objeto do componente Modal
   * @return string|Modal
   */

    private $class_botao;
    private $titulo_botao;
    private $class_modal;
    private $conteudo_body;
    private $conteudo_footer;
    private $id_modal;
    private $modal_label;
    private $modal_title;
    private $class_modal2;
    

    public function __construct($class_botao, $titulo_botao, $class_modal, $conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal2){
        $this->class_botao = $class_botao;
        $this->titulo_botao = $titulo_botao;
        $this->class_modal = $class_modal;
        $this->conteudo_body = $conteudo_body;
        $this->conteudo_footer = $conteudo_footer;
        $this->id_modal = $id_modal;
        $this->modal_label = $modal_label;
        $this->modal_title = $modal_title;
        $this->class_modal2 = $class_modal2;
      
      }


    public function getHTML(){
        $html = $this->botao();
        $html.= $this->modal();

        return $html.'</div>
        </div>
      </div>
    </div>';
    }

/**
 * Botao da classe do modal, onde você pode modificar o ID, o titulo e o a class para que seja personalizado para cada model
 * @return string|botaoModal
 */
    private function botao(){
        $html = '<button type="button" class="'.$this->class_botao.'" data-toggle="modal" data-target="#'.$this->id_modal.'">
        '.$this->titulo_botao.'
      </button>';

      return $html;
    }
/**
 * Classe do modal em si, onde se devine a class do modal, qeu seria o tamanho, lado,.. e seu nome titulo e conteudo
 * @return string|modal
 */

    private function modal(){
        $html = '<div class="'.$this->class_modal2.'" id="'.$this->id_modal.'" tabindex="-1" role="dialog" aria-labelledby="'.$this->modal_label.'"
        aria-hidden="true">
        <div class="modal-dialog'.$this->class_modal.'" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="'.$this->modal_label.'">'.$this->modal_title.'</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
        $html .= '<div class="modal-body">'.$this->conteudo_body.'
      </div>';
      $html .='<div class="modal-footer">'.$this->conteudo_footer.'';
      return $html;
    }
  }
  ?>  

