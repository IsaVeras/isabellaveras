$(function() {
	//PROCURA LOGIN JS
	$("#login_form").submit(function() {

		$.ajax({
			type: "post",
			url: BASE_URL + "restrict/ajax_login",
			dataType: "json",
			data: $(this).serialize(),
			beforeSend: function() {
				clearErrors();
				$("#btn_login").parent().siblings(".help-block").html(loadingImg("Verificando..."));
			},
			success: function(json) {
				if (json["status"] == 1) {
					clearErrors();
					console.log(json);
					$("#btn_login").parent().siblings(".help-block").html(loadingImg("Logando..."));
					if(json['tp_login']==1){

						window.location = BASE_URL + "restrict";
					}
					if(json['tp_login']==2){

						window.location = BASE_URL + "profissional";
					}
					if(json['tp_login']==3){

						window.location = BASE_URL + "cliente";
					}
					
				} else {
					showErrors(json["error_list"]);
				}
			},
			
		})

		return false;
	})

})