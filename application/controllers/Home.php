<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('MarginModel', 'margin');
		
    }

	public function index()
	{
		$data['marginPadrao'] = $this->margin->marginPadrao();

		$this->template->show('home');
	
	}
}
