<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Hero extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('HeroModel', 'hero');
		$this->load->model('MarginModel', 'margin');
		
    }

	public function index()
	{
		/**
		 * Carregando as views do hero
		 * ['hero']= variavel dada no model
		 * hero nome do component, na libraries
		 * hero() função criada no model
		 * 
		 * @return string|hero
		 * 
		 */
		
		$data['hero'] = $this->hero->hero();		
		/**
		 * Carregando a view hero com o template criado na librarie
		 * @return string|hero
		 */
		$data['marginPadrao'] = $this->margin->marginPadrao();


		$this->template->show('hero', $data);

	
	}
}
?>