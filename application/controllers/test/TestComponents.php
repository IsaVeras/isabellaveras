<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');


class TestComponents extends MyToast{

    function __construct(){
        parent:: __construct('TestComponents');
        $this->load->view('template/header');
    }

    function test_check_class_botao(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Modal Grande";
        $class_modal = " modal-lg";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'idmodal';
        $modal_label = 'labelmodal';
        $modal_title = 'GRANDE';
        $class_modal2 = 'modal fade';    

        $this->_assert_not_empty($class_botao,"A classe não esta preenchida");

    }


    function test_check_class_modal(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Modal Grande";
        $class_modal = " modal-lg";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'idmodal';
        $modal_label = 'labelmodal';
        $modal_title = 'GRANDE';
        $class_modal2 = 'modal fade';    

        $this->_assert_not_empty($class_modal,"A classe não esta preenchida");

    }

    function test_check_class_modal2(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Modal Grande";
        $class_modal = " modal-lg";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'idmodal';
        $modal_label = 'labelmodal';
        $modal_title = 'GRANDE';
        $class_modal2 = 'modal fade';    

        $this->_assert_not_empty($class_modal2,"A classe não esta preenchida");

    }


    function test_check_class_midia(){
        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
        vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
        congue felis in faucibus.';

        $this->_assert_not_empty($class_media,"A classe não esta preenchida");

    }

    function test_check_class_midia_teste2(){
        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
        vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
        congue felis in faucibus.';

        $this->_assert_empty($class_media,"A classe não esta preenchida");

    }

    function test_check_class_h5(){
        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
        vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
        congue felis in faucibus.';

        $this->_assert_not_empty($class_h5,"A classe não esta preenchida");

    }

    function test_check_img(){
        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
        vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
        congue felis in faucibus.';

        $this->_assert_not_empty($img,"Não possui imagem");

    }
    


    function test_check_class(){
        $titulo = 'EXEMPLO';
        $subtexto = 'ISABELLA VERAS ALVES SOUZA';
        $class = ' btn btn-secondary';
        $botao = 'CLIQUE AQUI';
        $link = ' https://fantoy.com.br/bonecos-colecionaveis/funko-pop.html';

        $this->_assert_not_empty($class,"A classe não esta preenchida");

    }

    function test_check_link(){
        $titulo = 'EXEMPLO';
        $subtexto = 'ISABELLA VERAS ALVES SOUZA';
        $class = ' btn btn-secondary';
        $botao = 'CLIQUE AQUI';
        $link = ' https://fantoy.com.br/bonecos-colecionaveis/funko-pop.html';

        $this->_assert_not_empty($link,"Não Possui link");

    }


    




}

?>