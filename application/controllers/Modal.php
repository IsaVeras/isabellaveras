<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modal extends CI_Controller {

	/**
	 * Constrindo o a classe (objeto) modal.
	 * @return string|modal
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('ModalModel', 'modal');
        $this->load->model('MarginModel', 'margin');
    }

	public function index()
	{
		/**
		 * Carregando as views do modal
		 * ['modal']= variavel dada no model
		 * modal nome do component, na libraries
		 * modal() função criada no model
		 * 
		 * @return string|modal
		 * 
		 */
		$data['modal'] = $this->modal->modal();

		$data ['modal2'] = $this->modal->modal2();
		
		$data['modal3'] = $this->modal->modal3(); 

		$data['modal4'] = $this->modal->modal4(); 
		
		$data['modalFrameTop'] = $this->modal->modalFrameTop(); 
		
		$data['modalFrameBottom'] = $this->modal->modalFrameBottom();

		$data['modalFluidRight'] = $this->modal->modalFluidRight();

		$data['modalFluidLeft'] = $this->modal->modalFluidLeft(); 
		
		$data['modalFluidBottom'] = $this->modal->modalFluidBottom(); 
		
		$data['modalFluidTop'] = $this->modal->modalFluidTop(); 
		
		$data['modalSideTopRight'] = $this->modal->modalSideTopRight(); 
		
		$data['modalSideTopLeft'] = $this->modal->modalSideTopLeft(); 

		$data['modalSideBottomLeft'] = $this->modal->modalSideBottomLeft(); 
		
		$data['modalSideBottomRight'] = $this->modal->modalSideBottomRight();

		$data['marginModal'] = $this->margin->marginModal();
		 


		/**
		 * Carregando a view modal com o template criado na librarie
		 * @return string|hero
		 */

		$this->template->show('modal', $data);

		

	
	}

}