<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Media extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('MediaModel', 'media');
		$this->load->model('MarginModel', 'margin');

    }

	public function index()
	{
		/**
		 * Carregando as views do media
		 * ['media']= variavel dada no model
		 * media nome do component, na libraries
		 * media() função criada no model
		 * 
		 * @return string|hero
		 * 
		 */
		$data['media'] = $this->media->media();

		$data['subMedia'] = $this->media->subMedia();

		$data['mediaTop'] = $this->media->mediaTop();

		$data['mediaCenter'] = $this->media->mediaCenter();

		$data['mediaBottom'] = $this->media->mediaBottom();

		$data['marginPadrao'] = $this->margin->marginPadrao();



		/**
		 * Carregando a view media com o template criado na librarie
		 * @return string|media
		 */

		$this->template->show('media', $data);

	
	}
}
?>