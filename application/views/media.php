<?=$marginPadrao?>   
    <h3 class = "text-center"> MEDIA</h3>
    
    <p>Media é um componente que possibilita a criação de componentes repetitivos como comentarios de redes sociais e blogs </p>
    <p>O objeto de mídia ajuda a criar componentes complexos e repetitivos, nos quais algumas mídias são posicionadas ao lado de conteúdo que não envolve a mídia. Além disso, ele faz isso com apenas duas classes obrigatórias, graças ao flexbox.</p>
    <p>Abaixo está um exemplo de um único objeto de mídia. Apenas duas classes são necessárias: o wrapping <span class="text-danger">.media</span> e o class <span class="text-danger">.media-body</span> em torno do seu conteúdo. O preenchimento e a margem opcionais podem ser controlados por meio de utilitários de espaçamento.</p>

    <div class="border border-info">
    <?= $media ?> 
     </div>
  </div>
  </div><br><br>
  <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
      <h4 class="text-center">Código</h4>
  1.   <span><</span>div class="media">
  2.      <span><</span>img class="d-flex mr-3" src="https://mdbootstrap.com/img/Photos/Others/placeholder1.jpg" alt="Generic placeholder image">
  3.    <span><</span>div class="media-body">
  4.      <span><</span>h5 class="mt-0 font-weight-bold">Media heading<span><</span>/h5>
  5.      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum invulputate at,
           tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec laciniacongue felis in faucibus.
  6.    <span><</span>/div>
  7.  <span><</span>/div>
  
</pre><br><br>
<p>Também da para colocar medias dentro de medias ficando parecido com os comentários das redes sociais</p>
<div class="border border-info">
    <?= $media ?>
    <?= $subMedia?> 
    </div></div>
    </div></div>
  </div><br><br>
  <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
      <h4 class="text-center">Código</h4>
  1.    <span><</span>div class="media">
  2.      <span><</span>img class="d-flex mr-3" src="https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg" alt="Generic placeholder image">
  3.      <span><</span>div class="media-body">
  4.        <span><</span>h5 class="mt-0 font-weight-bold">Media heading<span><</span>/h5>
  5.        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
          vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
          congue felis in faucibus.

  6.        <span><</span>div class="media mt-4">
  7.          <span><</span>a class="d-flex pr-3" href="#">
  8.            <span><</span>img src="https://mdbootstrap.com/img/Photos/Others/placeholder6.jpg" alt="Generic placeholder image">
  9.          <span><</span>/a>
  10.          <span><</span>div class="media-body">
  11.            <span><</span>h5 class="mt-0 font-weight-bold">Media heading</h5>
  12.            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
  13.            vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
  14.            congue felis in faucibus.
  15.          <span><</span>/div>
  16.        <span><</span>/div>
  17.      <span><</span>/div>
  18.    <span><</span>/div>
  </pre>

  <h2 class="text-secondary text-uppercase text-center"><strong>alinnhamento da imagem</strong></h2>
  <p>A imagem do componente pode ser fixada no começo do texto, no meio e o final. Como padrão é fixado em cima.</p>
  <ul>
  <li><span class="text-danger bg-light">align-self-center</span>Centro</li>
  <li><span class="text-danger bg-light">align-self-end</span> Final</li>
  <li><span class="text-danger bg-light">align-self-start</span> Meio</li>
  </ul>
  <div class="border border-info">
  <?=$mediaCenter?>
  </div></div>
  </div><br><br>

  <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
      <h4 class="text-center">Código</h4>
  1.    <span><</span>div class="media">
  2.    <span><</span>img class="d-flex align-self-center mr-3" src="https://mdbootstrap.com/img/Photos/Others/placeholder5.jpg" alt="Generic placeholder image">
  3.      <span><</span>div class="media-body">
  4.        <span><</span>h5 class="mt-0 font-weight-bold">Bottom-aligned media<span><</span>/h5>
  5.        <span><</span>p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus
  6.        odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
  7.        Donec lacinia congue felis in faucibus.<span><</span>/p>
  8.        <span><</span>p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus
  9.        et magnis dis parturient montes, nascetur ridiculus mus.<span><</span>/p>
  10.    <span><</span>/div>
  11.  <span><</span>/div>
  </pre><br><br>

  <div class="border border-info">
  <?=$mediaTop?>
  </div></div>
  </div><br><br>

  <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
      <h4 class="text-center">Código</h4>
  1.    <span><</span>div class="media">
  2.    <span><</span>img class="d-flex align-self-start mr-3" src="https://mdbootstrap.com/img/Photos/Others/placeholder5.jpg" alt="Generic placeholder image">
  3.      <span><</span>div class="media-body">
  4.        <span><</span>h5 class="mt-0 font-weight-bold">Bottom-aligned media<span><</span>/h5>
  5.        <span><</span>p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus
  6.        odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
  7.        Donec lacinia congue felis in faucibus.<span><</span>/p>
  8.        <span><</span>p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus
  9.        et magnis dis parturient montes, nascetur ridiculus mus.<span><</span>/p>
  10.    <span><</span>/div>
  11.  <span><</span>/div>
  </pre><br><br>

  <div class="border border-info">
  <?=$mediaBottom?>
  </div></div>
  </div><br><br>
  <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
      <h4 class="text-center">Código</h4>
  1.    <span><</span>div class="media">
  2.    <span><</span>img class="d-flex align-self-end mr-3" src="https://mdbootstrap.com/img/Photos/Others/placeholder5.jpg" alt="Generic placeholder image">
  3.      <span><</span>div class="media-body">
  4.        <span><</span>h5 class="mt-0 font-weight-bold">Bottom-aligned media<span><</span>/h5>
  5.        <span><</span>p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus
  6.        odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
  7.        Donec lacinia congue felis in faucibus.<span><</span>/p>
  8.        <span><</span>p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus
  9.        et magnis dis parturient montes, nascetur ridiculus mus.<span><</span>/p>
  10.    <span><</span>/div>
  11.  <span><</span>/div>
  </pre>
 

</div> 