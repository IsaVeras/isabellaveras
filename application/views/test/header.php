<html>
<head>
<title>LPII - Unit test results</title>
<style type="text/css">
* { font-family: Arial, sans-serif; font-size: 9pt }
#results { width: 100% }
.err, .pas { color: white; font-weight: bold; margin: 5px 0; padding: 5px; vertical-align: top; }
.err { background-color: #cc0000; border-radius:5px;}
.pas { background-color: #00b3b3; border-radius:5px; }
.detail { padding: 8px 0 8px 20px }
h1 { font-size: 12pt }
a:link, a:visited { text-decoration: none; color: white }
a:active, a:hover { text-decoration: none; color: #404040; background-color: #f2f2f2; border-radius:3px; } 
</style>
</head>
<body>

<h1>Toast - CodeIgniter Unit Test Plugin for LPII</h1><br>

<ol>
