<?=$marginPadrao?>
    <h3 class = "text-center"> HERO IMAGE</h3>      
    <?= $hero?> <br><br><br>
    <p>O hero image é um componente que serve para adicionar informações na imagens, ele é muito utilizado em e-commerce.</p>
    <p>Para utilizar o hero image temos que utlizar códigos em HTML e em CSS</p>
    <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
        <h4 class="text-center">Código HTML</h4>
    1.    <span><</span>div class="hero-image">
    2.        <span><</span>div class="hero-text">
    3.            <span><</span>h1>ISABELLA<span><</span>/h1>
    4.            <span><</span>p>VERAS ALVES SOUZA<span><</span>/p>
    5.            <span><</span>a href="https://fantoy.com.br/bonecos-colecionaveis/funko-pop.html"><span><</span>button>CLIQUE AQUI<span><</span>/a><span><</span>/button>
    6.        <span><</span>/div>
    7.    <span><</span>/div>
    </pre>
    <br><br>
    <p>No código do CSS, é bom utlizar o linear-gradient porque ele vai facilitar na leitura dos textos que vão ser colocados na imagem. </p>
    <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
        <h4 class="text-center">Código CSS</h4>
    1.    body, html {
    2.        height: 100%;
    3.    }
    4.
    5.    .hero-image {
    6.    background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("photographer.jpg");
    7. 
    8.    height: 50%;
    9.    
    10.    background-position: center;
    11.    background-repeat: no-repeat;
    12.    background-size: cover;
    13.    position: relative;
    14.   }
    15.    
    16.    .hero-text {
    17.    text-align: center;
    18.    position: absolute;
    19.    top: 50%;
    20.   left: 50%;
    21.    transform: translate(-50%, -50%);
    22.    color: white;
    23.    }
    </pre>
</div>