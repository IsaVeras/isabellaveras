<?=$marginModal?>    
    
    <h3 class = "text-center"> MODAL</h3>

    <p >O modal também é conhecido como PopUp, ele é uma janela que possui algum tipo de conteudo, seja ele apenas informativo
    ou um formulário ou até mesmo a janela de fazer loggin.</p>
    <p>Além de possuir várias funções ele também possui tamanhos, tipos e posições diferentes.</p>
    <p>Nessse conteudo vamos trazer alguns desses modelos e variaveis do modal.</p>


    
    <h2 class="text-secondary text-uppercase text-center"><strong>tamanhos</strong></h2>
    <p>Os tamanhos que ele possui são 3, pequeno, grande e tela inteira. O tamanho é definido pelo o que você coloca
      na 'class'.<div> <ul> Eles são definidos:</p>
                  <li><span class="text-danger bg-light">.modal-sm</span> Para o modal pequeno</li>
                  <li><span class="text-danger bg-light">.modal-lg</span> Para o modal grande</li>
                  <li><span class="text-danger bg-light">.modal-fluid</span> Para o modal tela inteia</li>
                  <li>Para o modal de tamanho médio, basta você não declarar tamanho</li>
                  </ul>
    <div class="text-center border border-warning rounded-pill">
      <h5 class="text-center  "style ="margin-top: 10px;">Vizualize os tamanhos</h5>
        <?= $modal?>    
        <?= $modal2?>    
        <?= $modal3?>    
        <?= $modal4?>
      </div>  <br><br><br>
       
      <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
         <h4 class="text-center">Código</h4>
    1.      <span><</span>button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal">
    2.          Launch demo modal
    3.      <<span>/button></span>
    4.
    5.      <span><</span>div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    6.      <span>  <</span>div class="modal-dialog" role="document">
    7.      <span>    <</span>div class="modal-content">
    8.      <span>      <</span>div class="modal-header">
    9.      <span>          <</span>h5 class="modal-title" id="exampleModalLabel">Modal title<span><</span>/h5>
    10.      <span>          <</span>button type="button" class="close" data-dismiss="modal" aria-label="Close">
    11.      <span>             <</span>span aria-hidden="true"><span>&</span>times;<span><</span>/span>
    12.      <span>          <</span>/button>
    13.      <span>      <</span>/div>
    14.      <span>      <</span>div class="modal-body">
    15.                    ...
    16.     <span>      <</span>/div>
    17.          <span>  <</span>div class="modal-footer">
    18.          <span>  <</span>button type="button" class="btn btn-secondary" data-dismiss="modal">Close<span><</span>/button>
    19.         <span>  <</span>button type="button" class="btn btn-primary">Save changes</button>
    20.       <span>  <</span>/div>
    21.      <span>  <</span>/div>
    22.    <span>  <</span>/div>
    23.  <span>  <</span>/div>
        </pre><br><br>



        <h2 class="text-secondary text-uppercase text-center"><strong>frame modal</strong></h2>
        <p>Para que o modal Frame possa funcionar corretamente precisa-se que antes de definir a posição precisa-se indicar que é um frame modal. Para indicar usa-se <span class="text-danger">modal-frame.</span></p>
        <p>O frame modal ele pode ter duas posições em cima <span class="text-danger">top</span> e em baixo <span class="text-danger">bottom</span></p>
        <ul>Então a o código ficará:
        <li><span class="text-danger bg-light">.modal-frame</span> + <span class="text-danger bg-light">.modal-bottom</span></li>
        <li><span class="text-danger bg-light">.modal-frame</span> + <span class="text-danger bg-light">.modal-top</span></li>
        </ul>
        <div class="text-center border border-warning rounded-pill">
      <h5 class="text-center  "style ="margin-top: 10px;">Vizualize os exemplos</h5>
          <?= $modalFrameTop?>   
          <?= $modalFrameBottom?> 
          </div><br><br><br>

          <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
         <h4 class="text-center">Código</h4>        
      1.  <span><</span>div class="modal fade bottom" id="frameModalBottom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel aria-hidden="true">
      2.    <span><</span>div class="modal-dialog modal-frame modal-bottom" role="document">
      3.     <span><</span>div class="modal-content">
      4.        <span><</span>div class="modal-body">
      5.          <span><</span>div class="row d-flex justify-content-center align-items-center">
      6.            <span><</span>p class="pt-3 pr-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit nisi quo provident fugiat reprehenderit nostrum quos..<span><</span>/p>
      7.              <span><</span>button type="button" class="btn btn-secondary" data-dismiss="modal">Close<span><</span>/button>
      8.              <span><</span>button type="button" class="btn btn-primary">Save changes<span><</span>/button>
      9.          <span><</span>/div>
      10.       <span><</span>/div>
      11.     <span><</span>/div>
      12.    <span><</span>/div>
      13.  <span><</span>/div>

        </pre><br><br>
        <h2 class="text-secondary text-uppercase text-center"><strong>fluid modal</strong></h2>
        <p>O fluid modal ele possui 4 posicões sendo, esquerda, direita, em cima e em baixo. Antes de declarar o lado tem que indicar que é um modal fluid com<span class="text-danger">.modal-full-height</span></p>
        <ul>Então a o código ficará:
        <li><span class="text-danger bg-light">.modal-full-height</span> + <span class="text-danger bg-light">.modal-bottom</span> Em baixo</li>
        <li><span class="text-danger bg-light">.modal-full-height</span> + <span class="text-danger bg-light">.modal-top</span> Em cima</li>
        <li><span class="text-danger bg-light">.modal-full-height</span> + <span class="text-danger bg-light">.modal-right</span> Direita</li>
        <li><span class="text-danger bg-light">.modal-full-height</span> + <span class="text-danger bg-light">.modal-left</span> Esquerda</li>
        </ul>
        
        <div class="text-center border border-warning rounded-pill">
          <h5 class="text-center  "style ="margin-top: 10px;">Vizualize os exemplos</h5>
          <?= $modalFluidRight?>   
          <?= $modalFluidLeft?>   
          <?= $modalFluidBottom?>   
          <?= $modalFluidTop?> 
        </div><br><br><br>
          

          <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
            <h4 class="text-center">Código</h4>
1.  <span><</span>div class="modal fade right" id="fullHeightModalRight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
2.    <span><</span>div class="modal-dialog modal-full-height modal-right" role="document">
3.      <span><</span>div class="modal-content">
4.       <span><</span>div class="modal-header">
5.        <span><</span>h4 class="modal-title w-100" id="myModalLabel">Modal title<span><</span>/h4>
6.        <span><</span>button type="button" class="close" data-dismiss="modal" aria-label="Close">
7.          <span><</span>span aria-hidden="true"><span>&</span>times;<span><</span>/span>
8.        <span><</span>/button>
9.      <span><</span>/div>
10.      <span><</span>div class="modal-body">
11.        ...
12.      <span><</span>/div>
13.      <span><</span>div class="modal-footer justify-content-center">
14.        <span><</span>button type="button" class="btn btn-secondary" data-dismiss="modal">Close<span><</span>/button>
15.        <span><</span>button type="button" class="btn btn-primary">Save changes<span><</span>/button>
16.      <span><</span>/div>
17.    <span><</span>/div>
  
  </pre><br><br>
        <h2 class="text-secondary text-uppercase text-center"><strong>side modal</strong></h2>
        <p>O fluid modal ele possui 4 posicões sendo, esquerda, direita, em cima e em baixo. Antes de declarar o lado tem que indicar que é um modal side com<span class="text-danger">.modal-side</span></p>
        <ul>Então a o código ficará:
        <li><span class="text-danger bg-light">.modal-side</span> + <span class="text-danger bg-light">.modal-bottom</span> Em baixo</li>
        <li><span class="text-danger bg-light">.modal-side</span> + <span class="text-danger bg-light">.modal-top</span> Em cima</li>
        <li><span class="text-danger bg-light">.modal-side</span> + <span class="text-danger bg-light">.modal-right</span> Direita</li>
        <li><span class="text-danger bg-light">.modal-side</span> + <span class="text-danger bg-light">.modal-left</span> Esquerda</li>
        </ul>
  <div class="text-center border border-warning rounded-pill">
      <h5 class="text-center  "style ="margin-top: 10px;">Vizualize os exemplos</h5>
      
          <?= $modalSideBottomRight?>   
          <?= $modalSideBottomLeft?>   
          <?= $modalSideTopLeft?>   
          <?= $modalSideTopRight?>  
</div><br><br><br>
          
        <pre class="grey lighten-3 px-3 mb-0 line-numbers rounded mb-0 language-html">
          <h4 class="text-center">Código</h4> 
        1.  <span><</span>div class="modal fade right" id="sideModalTR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        2.    <span><</span>div class="modal-dialog modal-side modal-top-right" role="document">
        3.      <span><</span>div class="modal-content">
        4.        <span><</span>div class="modal-header">
        5.          <span><</span>h4 class="modal-title w-100" id="myModalLabel">Modal title<span><</span>h4>
        6.          <span><</span>button type="button" class="close" data-dismiss="modal" aria-label="Close">
        7.            <span><</span>span aria-hidden="true<span>&</span>times<span><</span>/span>
        8.            <span><</span>/button>
        9.          <span><</span>/div>
        10.        <span><</span>div class="modal-body">
        11.          ...
        12.        <span><</span>/div>
        13.        <span><</span>div class="modal-footer">
        14.          <span><</span>button type="button" class="btn btn-secondary" data-dismiss="modal">Close<span><</span>/button>
        15.          <span><</span>button type="button" class="btn btn-primary">Save changes<span><</span>/button>
        16.        <span><</span>/div>
        17.      <span><</span>/div>
        18.    <span><</span>/div>
        19.  <span><</span>/div>
        </pre>
      
    </div>
    
</div>