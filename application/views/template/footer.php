<!--Footer-->
<footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">


<!--/.Call to action-->

<hr class="my-4">



<!--Copyright-->
<div class="footer-copyright py-3">
  © 2019 Copyright:  Isabella Veras
</div>
<!--/.Copyright-->

</footer>
<!--/.Footer-->