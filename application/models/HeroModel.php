<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Hero.php';
/**
 * Cria os models de Hero 
 * @return string|hero
 */

class HeroModel extends CI_Model{
    
    public function hero(){
        /**
         * Declaracao dos valores das variaveis criadas em librarie/component/Hero.php
         * @return string|hero
         */
        
        $titulo = 'EXEMPLO';
        $subtexto = 'ISABELLA VERAS ALVES SOUZA';
        $class = ' btn btn-secondary';
        $botao = 'CLIQUE AQUI';
        $link = ' https://fantoy.com.br/bonecos-colecionaveis/funko-pop.html';

        $hero = new HeroImage( $titulo, $subtexto, $class, $botao,$link);
        
        return $hero->getHTML();


    }

    

   
}?>

