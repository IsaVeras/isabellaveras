<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Margin.php';

/**
 * Cria os models de modal 
 * @return string|modal
 */

class MarginModel extends CI_Model{

    public function marginModal(){

        $value_top = '100px';
        $value_right = '50px';
        $value_left = '50px';
        $value_bottom = '100px';


        $marginModal = new MarginL($value_top, $value_right,$value_left,$value_bottom);

        return  $marginModal->getHtml();
    }

    public function marginPadrao(){

        $value_top = '100px';
        $value_right = '50px';
        $value_left = '50px';
        $value_bottom = '200px';


        $marginPadrao = new MarginL($value_top, $value_right,$value_left,$value_bottom);

        return  $marginPadrao->getHtml();
    }

    
    
}