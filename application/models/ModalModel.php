<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Modal.php';

/**
 * Cria os models de modal 
 * @return string|modal
 */

class ModalModel extends CI_Model{
    
    //TAMANHOS DE MODAL
    public function modal(){

        /**
         * Declaracao dos valores das variaveis criadas em librarie/component/Modal.php
         * @return string|modal
         */
            
        $class_botao = " btn btn-primary";
        $titulo_botao = "Modal Grande";
        $class_modal = " modal-lg";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'idmodal';
        $modal_label = 'labelmodal';
        $modal_title = 'GRANDE';
        $class_modal2 = 'modal fade';


        $modal = new ModalL($class_botao, $titulo_botao, $class_modal, $conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal2);
        
        return $modal->getHTML();


    }

    public function modal2(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Modal pequeno";
        $class_modal = " modal-sm";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn-sm btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn-sm btn-primary">Save changes</button>';
        $id_modal = 'idmodal2';
        $modal_label = 'labelmodal2';
        $modal_title = 'PEQUENO';
        $class_modal2 = 'modal fade';

        $modal2 = new ModalL ($class_botao, $titulo_botao,$class_modal,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal2);

        return $modal2->getHTML();
    }

    public function modal3(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Modal tela inteira";
        $class_modal = " modal-frame modal-top";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'idmodal3';
        $modal_label = 'labelmodal3';
        $modal_title = 'TELA INTEIRA';
        $class_modal2 = 'modal fade';

        $modal3 = new ModalL ($class_botao, $titulo_botao,$class_modal,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal2);

        return $modal3->getHTML();

    }

    public function modal4(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Modal médio";
        $class_modal = " ";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'idmodal4';
        $modal_label = 'labelmodal4';
        $modal_title = 'MÉDIO';
        $class_modal2 = 'modal fade';

        $modal4 = new ModalL ($class_botao, $titulo_botao,$class_modal,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal2);

        return $modal4->getHTML();

    }
    //TIPOS DE MODAL

    //FRAME
    public function modalFrameTop(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Frame Top";
        $class_modal = " modal fade top";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalFTId';
        $modal_label = 'modalFTLabel';
        $modal_title = 'Frame Top';
        $class_modal2 = ' modal-frame modal-top';


        
        $modalFrameTop = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalFrameTop->getHTML();

    }

    public function modalFrameBottom(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Frame Bottom";
        $class_modal = " modal fade bottom";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalFBId';
        $modal_label = 'modalFBLabel';
        $modal_title = 'Frame Bottom';
        $class_modal2 = ' modal-frame modal-bottom';


        
        $modalFrameBottom = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalFrameBottom->getHTML();

    }

    //FLUID

    public function modalFluidRight(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Fluid Right";
        $class_modal = " modal fade right";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalFluidRId';
        $modal_label = 'modalFluidRLabel';
        $modal_title = 'Fluid Right';
        $class_modal2 = ' modal-full-height modal-right';


        
        $modalFluidRight = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalFluidRight->getHTML();

    }

    public function modalFluidLeft(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Fluid Left";
        $class_modal = " modal fade left";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalFluidLId';
        $modal_label = 'modalFluidLLabel';
        $modal_title = 'Fluid Left';
        $class_modal2 = ' modal-full-height modal-left';


        
        $modalFluidLeft = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalFluidLeft->getHTML();

    }

    public function modalFluidBottom(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Fluid Bottom";
        $class_modal = " modal fade bottom";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalFluidBId';
        $modal_label = 'modalFluidBLabel';
        $modal_title = 'Fluid Bottom';
        $class_modal2 = ' modal-full-height modal-bottom';


        
        $modalFluidBottom = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalFluidBottom->getHTML();

    }

    public function modalFluidTop(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Fluid Top";
        $class_modal = " modal fade top";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalFluidTId';
        $modal_label = 'modalFluidTLabel';
        $modal_title = 'Fluid Top';
        $class_modal2 = ' modal-full-height modal-top';


        
        $modalFluidTop = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalFluidTop->getHTML();

    }


    //SIDE

    public function modalSideTopRight(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Top Right";
        $class_modal = " modal fade right";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalSideTRId';
        $modal_label = 'modalSideTRLabel';
        $modal_title = 'Top Right';
        $class_modal2 = ' modal-side modal-top-right';


        
        $modalSideTopRight = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalSideTopRight->getHTML();

    }

    public function modalSideBottomRight(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Bottom Right";
        $class_modal = " modal fade right";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalSideBRId';
        $modal_label = 'modalSideBRLabel';
        $modal_title = 'Bottom Right';
        $class_modal2 = ' modal-side modal-bottom-right';


        
        $modalSideBottomRight = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalSideBottomRight->getHTML();

    }

    public function modalSideTopLeft(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Top Left";
        $class_modal = " modal fade left";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalSTLId';
        $modal_label = 'modalSTLLabel';
        $modal_title = 'Top Left';
        $class_modal2 = ' modal-side modal-top-left';


        
        $modalSideTopLeft = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalSideTopLeft->getHTML();

    }

    public function modalSideBottomLeft(){
        $class_botao = " btn btn-primary";
        $titulo_botao = "Bottom Left";
        $class_modal = " modal fade left";
        $conteudo_body = "...";
        $conteudo_footer = '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>';
        $id_modal = 'modalSBLId';
        $modal_label = 'modalSBLLabel';
        $modal_title = 'Bottom Left';
        $class_modal2 = ' modal-side modal-bottom-left';


        
        $modalSideBottomLeft = new ModalL ($class_botao, $titulo_botao,$class_modal2,$conteudo_body,$conteudo_footer,$id_modal,$modal_label,$modal_title,$class_modal);
        
        return $modalSideBottomLeft->getHTML();

    }







}
?>