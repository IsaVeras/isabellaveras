<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Media.php';

/**
 * Cria os models de Media 
 * @return string|media
 */

class MediaModel extends CI_Model{
    
    public function media(){
        
        /**
         * Declaracao dos valores das variaveis criadas em librarie/component/Media.php
         * @return string|media
         */

        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
        vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
        congue felis in faucibus.';

        $media = new MediaL( $class_media, $top_value, $right_value, $left_value,$bottom_value, $class_image, $img, $alt, $class_h5, $title, $text);
        
        return $media->getHTML();


    }

    public function subMedia(){

        $class_media = ' mt-4';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Sub Media';
        $text = 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in
        vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia
        congue felis in faucibus.';

        $subMedia = new MediaL( $class_media, $top_value, $right_value, $left_value,$bottom_value, $class_image, $img, $alt, $class_h5, $title, $text);
        
        return $subMedia->getHTML();

    }

    public function mediaCenter(){

        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex align-self-center mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = '<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus
        odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
        Donec lacinia congue felis in faucibus.</p>
      <p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus
        et magnis dis parturient montes, nascetur ridiculus mus.</p>';

        $mediaCenter = new MediaL( $class_media, $top_value, $right_value, $left_value,$bottom_value, $class_image, $img, $alt, $class_h5, $title, $text);
        
        return $mediaCenter->getHTML();

    }

    public function mediaTop(){

        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex align-self-start mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder4.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = '<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus
        odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
        Donec lacinia congue felis in faucibus.</p>
      <p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus
        et magnis dis parturient montes, nascetur ridiculus mus.</p>';

        $mediaTop = new MediaL( $class_media, $top_value, $right_value, $left_value,$bottom_value, $class_image, $img, $alt, $class_h5, $title, $text);
        
        return $mediaTop->getHTML();

    }
    
    public function mediaBottom(){

        $class_media = '';
        $top_value = '10px';
        $right_value = '50px';
        $left_value = '50px';
        $bottom_value = '5px';
        $class_image = 'd-flex align-self-end mr-3';
        $img = 'https://mdbootstrap.com/img/Photos/Others/placeholder5.jpg';
        $alt = 'Generic placeholder image';
        $class_h5 = 'mt-0 font-weight-bold';
        $title = 'Media heading';
        $text = '<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus
        odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
        Donec lacinia congue felis in faucibus.</p>
      <p class="mb-0">Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus
        et magnis dis parturient montes, nascetur ridiculus mus.</p>';

        $mediaBottom = new MediaL( $class_media, $top_value, $right_value, $left_value,$bottom_value, $class_image, $img, $alt, $class_h5, $title, $text);
        
        return $mediaBottom->getHTML();

    }

   
}