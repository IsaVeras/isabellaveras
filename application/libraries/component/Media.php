<?php
include_once 'Component.php';

 /**
   * Classe cria o objeto do componente media
   * @return string|media
   */


class MediaL extends Component{

    private $class_media;
    private $top_value;
    private $right_value;
    private $left_value;
    private $bottom_value;
    private $class_image;
    private $img;
    private $alt;
    private $class_h5;
    private $title;
    private $text;

    public function __construct($class_media, $top_value, $right_value, $left_value,$bottom_value, $class_image, $img, $alt, $class_h5, $title, $text){
        $this->class_media = $class_media;
        $this->top_value = $top_value;
        $this->right_value = $right_value;
        $this->left_value = $left_value;
        $this->bottom_value = $bottom_value;
        $this->class_image = $class_image;
        $this->img = $img;
        $this->alt = $alt;
        $this->class_h5 = $class_h5;
        $this->title = $title;
        $this->text = $text;
      }

      public function getHTML(){
        $html = $this->media();
        return $html;
    }


    private function media(){

        $html ='<div class="media'.$this->class_media.'" style="margin-top:'.$this->top_value.'; margin-right:'.$this->right_value.'; margin-left:'.$this->left_value.'; margin-bottom: '.$this->bottom_value.';">';
        $html .= '<img class="'.$this->class_image.'" src="'.$this->img.'" alt="'.$this->alt.'">';
        $html .= '<div class="media-body">
        <h5 class="'.$this->class_h5.'">'.$this->title.'</h5>
        '.$this->text.'';
        return $html;

    }

   

}

?>