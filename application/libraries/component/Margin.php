<?php
include_once 'Component.php';
/**
 * Essa classe define as margens 
 * Deve-se fechar div até onde deseja as margens
 * @return string|Margin
 */

class MarginL extends Component{

    private $value_top;
    private $value_right;
    private $value_left;
    private $value_bottom;

    public function __construct($value_top,$value_right,$value_left,$value_bottom){
        $this->value_top = $value_top;
        $this->value_right = $value_right;
        $this->value_left  =$value_left;
        $this->value_bottom = $value_bottom;
    }


    public function getHTML(){
        $html = $this->margin();
        return $html;
    }

    private function margin(){

    $html = '<div style="margin-top:'.$this->value_top.'; margin-right:'.$this->value_right.'; margin-left:'.$this->value_left.'; margin-bottom: '.$this->value_bottom.';">';
    
    return $html;

    }

}
?>