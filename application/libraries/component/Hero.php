<?php
include_once 'Component.php';

class HeroImage extends Component{
  /**
   * Classe cria o objeto do componente heroImage
   * @return string|hero
   */

  private $titulo;
  private $subtexto;
  private $class;
  private $botao;
  private $link;

  public function __construct($titulo, $subtexto, $class, $botao,$link){
    $this->titulo = $titulo;
    $this->subtexto = $subtexto;
    $this->class = $class;
    $this->botao = $botao;
    $this->link = $link;
  }

  public function getHTML(){
          $html = $this->hero();
          return $html. '</div></div>';
      }

  private function hero(){
    $html = ' <div class="hero-image" >
                <div class="hero-text">';
    $html .= '<h1>'.$this->titulo.'</h1>
              <p>'.$this->subtexto.'</p>
              <a href="'.$this->link.'"><button class= "'.$this->class.'">'.$this->botao.'</button></a>';
    return $html;

  }
}


?>
  